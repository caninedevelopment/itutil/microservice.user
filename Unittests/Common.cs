﻿using ITUtil.Common.RabbitMQ.DTO;
using System;
using System.Collections.Generic;

namespace Unittests
{
    public class Common
    {
        public static Guid orgId = new Guid("59713bbd-1877-449e-ae5b-87a5018030f1");
        public static string[] configurations = { "appsettings.mssql.json", "appsettings.postgresql.json" };

        public static TokenInfo GetMockWrapper ()
        {
            return new TokenInfo()
            {
                claims = new List<TokenInfo.Claim>(),
                tokenId = Guid.Empty,
                userId = Guid.Empty,
                validUntil = DateTime.Now.AddHours(1)
            };
        }
    }
}
