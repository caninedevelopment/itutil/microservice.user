﻿using Application.User.V2.Database;
using Application.User.V2.Commands.Token;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Unittests
{
    [TestFixture]
    public class UserGroupQueueHandlerTest
    {
        [Test]
        public void AddToGroup_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                Application.User.V2.Commands.UserGroup.create createcmd = new Application.User.V2.Commands.UserGroup.create();
                var usergrp = new Application.User.V2.DTO.UserGroup()
                {
                    name = "test",
                    usergroupId = Guid.NewGuid(),
                    //organisationId = Common.orgId,
                    claims = new List<Application.User.V2.DTO.MetaData>()
                };
                createcmd.Execute(usergrp);


                //ACT
                Application.User.V2.Commands.UserGroup.addUserToUsergroup addcmd = new Application.User.V2.Commands.UserGroup.addUserToUsergroup();
                addcmd.Execute(
                    new Application.User.V2.DTO.ChangeUserGroup()
                    {
                        userId = login.userId,
                        usergroupId = usergrp.usergroupId
                    }, Common.GetMockWrapper(), ""
                    );
            }
        }

        [Test]
        public void Create_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                Application.User.V2.Commands.UserGroup.create createcmd = new Application.User.V2.Commands.UserGroup.create();
                createcmd.Execute(
                new Application.User.V2.DTO.UserGroup()
                {
                    name = "test",
                    usergroupId = Guid.NewGuid(),
                    //organisationId = Common.orgId,
                    claims = new List<Application.User.V2.DTO.MetaData>()
                });
            }
        }

        [Test]
        public void Delete_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                Application.User.V2.Commands.UserGroup.create createcmd = new Application.User.V2.Commands.UserGroup.create();
                var usergrp = new Application.User.V2.DTO.UserGroup()
                {
                    name = "test",
                    usergroupId = Guid.NewGuid(),
                    //organisationId = Common.orgId,
                    claims = new List<Application.User.V2.DTO.MetaData>()
                };
                createcmd.Execute(usergrp);

                //ACT
                Application.User.V2.Commands.UserGroup.delete deletecmd = new Application.User.V2.Commands.UserGroup.delete();
                deletecmd.Execute(new Application.User.V2.DTO.UserGroupId() { usergroupId = usergrp.usergroupId });
            }
        }


        [Test]
        public void Get_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                //PEPRAE

                //ACT
                Application.User.V2.Commands.UserGroup.getall getcmd = new Application.User.V2.Commands.UserGroup.getall();
                var res = getcmd.Execute(new object());
            }
        }

        [Test]
        public void Update_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                Application.User.V2.Commands.UserGroup.create createcmd = new Application.User.V2.Commands.UserGroup.create();
                var usergrp = new Application.User.V2.DTO.UserGroup()
                {
                    name = "test",
                    usergroupId = Guid.NewGuid(),
                    //organisationId = Common.orgId,
                    claims = new List<Application.User.V2.DTO.MetaData>()
                };
                createcmd.Execute(usergrp);

                //ACT
                Application.User.V2.Commands.UserGroup.update updatecmd = new Application.User.V2.Commands.UserGroup.update();
                updatecmd.Execute(new Application.User.V2.DTO.UserGroup()
                {
                  //  organisationId = Common.orgId,
                    claims = new List<Application.User.V2.DTO.MetaData>(),
                    name = "updated",
                    usergroupId = usergrp.usergroupId
                });
            }
        }

        //[Test]
        //public void Remove_Success()
        //{
        //    foreach (var configfile in Common.configurations)
        //    {
        //        DataContext dc = new DataContext(configfile);
        //        UserGroupQueueHandler.dbcontext = dc;
        //        var wrapper = Common.GetMockWrapper();
        //        //PEPRAE
        //        ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
        //        wrapper,
        //        new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
        //        );
        //        var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
        //        var login = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

        //        var tmpUserGrp = new Application.User.V2.DTO.UserGroup()
        //        {
        //            name = "old",
        //            usergroupId = Guid.NewGuid(),
        //            organisationId = Common.orgId,
        //            claims = new List<MetaData>()
        //        };
        //        ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.UserGroup>.SetData(wrapper, tmpUserGrp);
        //        UserGroupQueueHandler.ExecuteOperationCreate(wrapper);
                
        //        ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.ChangeUserGroup>.SetData(
        //            wrapper,
        //            new Application.User.V2.DTO.ChangeUserGroup()
        //            {
        //                userId = login.userId,
        //                usergroupId = tmpUserGrp.usergroupId
        //            }
        //            );
        //        UserGroupQueueHandler.ExecuteOperationAddToGroup(wrapper);

        //        //ACT
        //        var tmpDeleteUserGrp = new Application.User.V2.DTO.ChangeUserGroup()
        //        {
        //             userId = login.userId,
        //             usergroupId = tmpUserGrp.usergroupId
        //        };
        //        ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.ChangeUserGroup>.SetData(wrapper, tmpDeleteUserGrp);
        //        var res = UserGroupQueueHandler.ExecuteOperationRemoveFromGroup(wrapper);

        //        //RESOLVE
        //        Assert.IsTrue(res.success);
        //    }
        //}
    }
}
