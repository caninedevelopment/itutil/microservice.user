﻿using Application.User.V2.Database;
using NUnit.Framework;
using System;
using Application.User.V2.Commands.Token;

namespace Unittests
{
    [TestFixture]
    public class TokenQueueHandlerTests
    {
        [Test]
        public void Login_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var res = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });
                Assert.IsNotNull(res);
            }
        }

        [Test]
        public void Login_Fail()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                login cmd = new login();
                var res = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "nope" });
            }
        }

        [Test]
        public void Logout_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);

                //PEPRAE
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                //ACT
                logout logoutcmd = new logout();
                logoutcmd.Execute(new Application.User.V2.DTO.Token() { tokenId = login.tokenId });
            }
        }

        [Test]
        public void Logout_Fail()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);
                logout logoutcmd = new logout();
                logoutcmd.Execute(new Application.User.V2.DTO.Token() { tokenId = Guid.NewGuid() });
            }
        }

        [Test]
        public void Refresh_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);

                //PEPRAE
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                refresh refreshcmd = new refresh();
                var res = refreshcmd.Execute(new Application.User.V2.DTO.Token() { tokenId = login.tokenId });
            }
        }

        [Test]
        public void Refresh_Fail()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);

                refresh refreshcmd = new refresh();
                var res = refreshcmd.Execute(new Application.User.V2.DTO.Token() { tokenId = Guid.NewGuid()});
            }
        }

        [Test]
        public void Verify_Success()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);

                //PEPRAE
                login cmd = new login();
                var login = cmd.Execute(new Application.User.V2.DTO.Login() { username = "Jimmy", password = "defaultPWD" });

                verify verifycmd = new verify();
                var res = verifycmd.Execute(new Application.User.V2.DTO.Token() { tokenId = login.tokenId });
            }
        }

        [Test]
        public void Verify_Fail()
        {
            foreach (var configfile in Common.configurations)
            {
                DataContext.Instance = new DataContext(configfile);

                verify verifycmd = new verify();
                var res = verifycmd.Execute(new Application.User.V2.DTO.Token() { tokenId = Guid.NewGuid() });
            }
        }
    }
}