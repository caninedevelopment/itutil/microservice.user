﻿//using Application.User.V2.Database;
//using Application.User.V2.Commands.Token;
//using ITUtil.Common.DTO;
//using NUnit.Framework;
//using ServiceStack.OrmLite;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace Unittests
//{
//    [TestFixture]
//    public class UserQueueHandlerTest
//    {
//        [Test]
//        public void Create_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.CreateUser()
//                    {
//                        //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                        email = Guid.NewGuid().ToString() + "@test.test",
//                        metadata = new List<MetaData>(),
//                        mobile = "+45 1234 5678",
//                        organisationContext = Common.orgId,
//                        password = "1234",
//                        userId = Guid.NewGuid(),
//                        userName = "Test" + Guid.NewGuid(),
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void Delete_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);


//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.DeleteUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.DeleteUser()
//                    {
//                        organisationId = Common.orgId,
//                        userId = tmpusr.userId
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationDelete(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void Get_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);


//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.GetUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.GetUser()
//                    {
//                        //organisationId = Common.orgId,
//                        userId = tmpusr.userId
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationGet(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void Get_Claims_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);
                
//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.GetUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.GetUser()
//                    {
//                        userId = wrapper.headerInfo.userId
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationGet(wrapper);
//                var usr = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<Application.User.V2.DTO.User>(res.data);

//                var claim = usr.claims.Where(p => p.organisationId == Common.orgId).FirstOrDefault();
//                Assert.IsNotNull(claim);
//                Assert.Greater(claim.orgClaims.Count(), 0);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void Get_Fail()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.GetUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.GetUser()
//                    {
//                        //organisationId = Common.orgId,
//                        userId = Guid.NewGuid()
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationGet(wrapper);

//                //RESOLVE
//                Assert.IsFalse(res.success);
//            }
//        }

//        [Test]
//        public void GetAll_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);


//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.OrganisationId>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.OrganisationId()
//                    {
//                        organisationId = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationGetAll(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void GetMyProfile_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //ACT
//                var res = UserQueueHandler.ExecuteOperationGetMyProfile(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void Invite_Fail()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.InviteUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.InviteUser()
//                    {
//                        userName = "TestInvite" + Guid.NewGuid(),
//                        organisationContext = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationInvite(wrapper);

//                //RESOLVE
//                Assert.IsFalse(res.success);
//            }
//        }

//        [Test]
//        public void Invite_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.InviteUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.InviteUser()
//                    {
//                        userName = "Jimmy",
//                        organisationContext = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationInvite(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

        
//        [Test]
//        public void UpdateMyProfile_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.UpdateUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.UpdateUser()
//                    {
//                        userId = wrapper.headerInfo.userId,
//                        mobile = "+45 1234 9876",
//                        userName = "UPDATED BY MY PROFILE " + Guid.NewGuid(),
//                        organisationContext = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationUpdateMyData(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }

//        [Test]
//        public void UpdateMyProfile_Fail()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.UpdateUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.UpdateUser()
//                    {
//                        userId = tmpusr.userId,
//                        mobile = "+45 1234 9876",
//                        userName = "TestUpdate" + Guid.NewGuid(),
//                        organisationContext = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationUpdateMyData(wrapper);

//                //RESOLVE
//                Assert.IsFalse(res.success);
//            }
//        }

//        [Test]
//        public void Update_Success()
//        {
//            foreach (var configfile in Common.configurations)
//            {
//                DataContext dc = new DataContext(configfile);
//                UserGroupQueueHandler.dbcontext = dc;
//                var wrapper = Common.GetMockWrapper();

//                //PEPRAE
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.Login>.SetData(
//                wrapper,
//                new Application.User.V2.DTO.Login() { userName = "Jimmy", password = "defaultPWD" }
//                );
//                var loginres = TokenQueueHandler.ExecuteOperationLogin(wrapper);
//                wrapper.headerInfo = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<TokenData>(loginres.data);

//                var tmpusr = new Application.User.V2.DTO.CreateUser()
//                {
//                    //???-TODO: giver det mening? claims = new Application.User.V2.DTO.OrganisationClaims 
//                    email = Guid.NewGuid().ToString() + "@test.test",
//                    metadata = new List<MetaData>(),
//                    mobile = "+45 1234 5678",
//                    organisationContext = Common.orgId,
//                    password = "1234",
//                    userId = Guid.NewGuid(),
//                    userName = "Test" + Guid.NewGuid(),
//                };

//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.CreateUser>.SetData(wrapper, tmpusr);
//                UserQueueHandler.ExecuteOperationCreate(wrapper);

//                //ACT
//                ITUtil.Common.DTO.MessageWrapperHelper<Application.User.V2.DTO.UpdateUser>.SetData(
//                    wrapper,
//                    new Application.User.V2.DTO.UpdateUser()
//                    {
//                        userId = tmpusr.userId,
//                        mobile = "+45 1234 9876",
//                        userName = "TestUpdate" + Guid.NewGuid(),
//                        organisationContext = Common.orgId,
//                    }
//                    );
//                var res = UserQueueHandler.ExecuteOperationUpdate(wrapper);

//                //RESOLVE
//                Assert.IsTrue(res.success);
//            }
//        }
//    }
//}
