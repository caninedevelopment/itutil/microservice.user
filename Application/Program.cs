﻿namespace Application.User
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new V2.Commands.Token.Namespace(),
                new V2.Commands.TrustedAuth.Namespace(),
                new V2.Commands.User.Namespace(),
                new V2.Commands.UserGroup.Namespace(),
            },true);
        }
    }
}
