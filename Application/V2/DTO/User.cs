﻿// <copyright file="User.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>


namespace Application.User.V2.DTO
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// User definition
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public Guid userId { get; set; }

        /// <summary>
        /// Gets or sets Username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Gets or sets Mobile
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// Gets or sets Metadata
        /// </summary>
        public List<MetaData> metadata { get; set; }

        /// <summary>
        /// Gets or sets Organisations
        /// </summary>
        public OrganisationClaims claims { get; set; }
   }

    public class Users
    {
        public List<User> users {get;set;}
    }
}