﻿using ITUtil.Common.Base;

namespace Application.User.V2.DTO
{

    public class GatewayResponse
    {
        public LocalizedString[] message { get; set; }
        public bool success { get; set; }
        public string data { get; set; }
        public string dataType { get; set; }
        public string responseToClientId { get; set; }
        public string responseToMessageId { get; set; }
    }
}
