﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2.DTO
{
    public class Settings
    {
        /// <summary>
        /// Gets or sets name
        /// </summary>
        public int smtpPort { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpServerName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpUserName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpPassword { get; set; }

        public string anonymUserGroup { get; set; }

        public forgotPasswordSettings Template { get; set; }
    }

    public class forgotPasswordSettings
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SenderEmail { get; set; }
        public string NameOfSender { get; set; }
    }

}