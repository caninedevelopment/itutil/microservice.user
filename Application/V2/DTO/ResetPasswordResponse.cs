﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2.DTO
{
    public class ResetPasswordResponse
    {
        public string ResetToken { get; set; }
    }
}
