﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2.DTO
{
    public class ResetPassword
    {
        public string Email { get; set; }
    }
}
