﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2.DTO
{
    public class ResetpasswordByToken
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
