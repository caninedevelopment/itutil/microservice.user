﻿// <copyright file="Token.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Application.User.V2.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.User.V2.DTO;
    using ITUtil.Common.RabbitMQ.DTO;

    /// <summary>
    /// Login tokens, describes a login-token for security validation.
    /// </summary>
    public partial class Token
    {
        /// <summary>
        /// Gets or sets Unique token id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the user assosiated with the token.
        /// </summary>
        public Guid FK_User { get; set; }

        /// <summary>
        /// Gets or sets how long the token is valid.
        /// </summary>
        public DateTime ValidUntil { get; set; }

        public OrganisationClaims Claims { get; set; }

        internal static TokenInfo ConvertToTokenData(Token t)
        {
            var claims = new List<TokenInfo.Claim>();
            if (t.Claims.orgClaims != null)
                claims.AddRange(t.Claims.orgClaims.Select(p=> new TokenInfo.Claim() { key=p.key, scope= ITUtil.Common.Command.Claim.DataContextEnum.organisationClaims, value=p.value }));

            if (t.Claims.userClaims != null)
                claims.AddRange(t.Claims.userClaims.Select(p => new TokenInfo.Claim() { key = p.key, scope = ITUtil.Common.Command.Claim.DataContextEnum.userClaims, value = p.value }));

            return new TokenInfo()
            {
                claims = claims,
                tokenId = t.Id,
                userId = t.FK_User,
                validUntil = t.ValidUntil,
            };
        }
    }
}