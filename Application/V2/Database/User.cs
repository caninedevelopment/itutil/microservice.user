﻿// <copyright file="User.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Application.User.V2.Database
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Application.User.V2.DTO;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Informations about a given user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets MD5Password.
        /// </summary>
        public string PWDHash { get; set; }

        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets Mobile.
        /// </summary>
        public string Mobile { get; set; }

        public string ResetToken { get; set; }

        public List<TokenInfo.Claim> UserClaims { get; set; }

        /// <summary>
        /// Gets or sets Metadata.
        /// </summary>
        public virtual List<Application.User.V2.DTO.MetaData> Metadata { get; set; }

        /// <summary>
        /// Validates an email adress.
        /// </summary>
        /// <param name="email">the email to be validated</param>
        /// <returns>true if it is valid</returns>
        public static bool ValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email) == false)
            {
                Regex regex = new Regex(@"^(([\w-]+\.)+[\w-]+|([\w-]+\+)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                  + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]? [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                  + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]? [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                  + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$");
                Match match = regex.Match(email);
                return match.Success;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Validates a phone number.
        /// </summary>
        /// <param name="phone">the phone to be validated</param>
        /// <returns>true if it is valid</returns>
        public static bool ValidPhone(string phone)
        {
            if (string.IsNullOrEmpty(phone) == false)
            {
                Regex regex = new Regex(@"^\s*\+?[0-9]\d?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
                Match match = regex.Match(phone);
                return match.Success;
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Create an administrator account on the given organisation context.
        /// </summary>
        /// <param name="usr">The user details for the account.</param>
        /// <returns>The created/updated user.</returns>
        public static User CreateAdmin(IDbConnection db, Application.User.V2.DTO.User usr)
        {
            string pwd = "defaultPWD";
            var dbuser = db.Select<User>(p => p.Name == usr.username).FirstOrDefault();
            if (dbuser == null)
            {
                dbuser = new User()
                {
                    Id = usr.userId,
                    Name = usr.username,
                    Email = usr.email,
                    Mobile = usr.mobile,
                    PWDHash = HashHelper.CalculateHash(pwd, usr.userId.ToString()),
                };
                db.Insert<User>(dbuser);


                var usergrp = new UserGroup()
                {
                    Id = default(Guid),
                    Name = "Admin",
                    Claims = new List<MetaData>()
                {
                    new MetaData()
                    {
                        key = Commands.User.Namespace.IsAdmin.key,
                        value = "true",
                    },
                },
                };

                if (db.Select<UserGroup>(p => p.Id == usergrp.Id).Any() == false)
                {
                    db.Insert<UserGroup>(usergrp);
                }

                UserInUserGroup uiug = new UserInUserGroup() { FK_User = dbuser.Id, FK_UserGroup = usergrp.Id };
                db.Insert<UserInUserGroup>(uiug);
            }
            return dbuser;
        }
    }
}