﻿// <copyright file="UserLoginLog.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Application.User.V2.Database
{
    using ServiceStack.DataAnnotations;
    using System;

    /// <summary>
    /// UserLoginLog
    /// </summary>
    public class UserLoginLog
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        [AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets EventDate
        /// </summary>
        public DateTime EventDate { get; set; }

        /// <summary>
        /// Gets or sets User
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets Ip
        /// </summary>
        public string Ip { get; set; }
    }
}