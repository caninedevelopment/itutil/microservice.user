﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2.Database
{
    public class Settings
    {
        public int smtpPort { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpServerName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpUserName { get; set; }

        /// <summary>
        /// Gets or sets nameOfSender
        /// </summary>
        public string smtpPassword { get; set; }

        public string ForgotPasswordEmailSubject { get; set; }
        public string ForgotPasswordEmailBody { get; set; }
        public string ForgotPasswordSenderEmail { get; set; }
        public string ForgotPasswordNameOfSender { get; set; }

        public string anonymUserGroup { get; set; }
    }
}
