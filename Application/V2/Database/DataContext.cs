﻿// <copyright file="DataContext.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Application.User.V2.Database
{
    using System;
    using ITUtil.Common.Config;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Datacontext for the userdatabase.
    /// </summary>
    public class DataContext
    {

        private static DataContext dbcontext = null;

        public static DataContext Instance
        {
            get
            {
                if (dbcontext == null)
                {
                    dbcontext = new DataContext();
                }

                return dbcontext;
            }

            set
            {
                dbcontext = value;
            }
        }

        public OrmLiteConnectionFactory DbFactory = null;
        private string dbName = "user";

        /// <summary>
        /// Initializes a new instance of the <see cref="DataContext"/> class.
        /// </summary>
        /// <param name="settingsFilename"></param>
        public DataContext(string settingsFilename = null)
        {
            var settings = ITUtil.Common.Config.GlobalRessources.getConfig(settingsFilename).GetSetting<SQLSettings>();
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, this.dbName);
            }

            // set the factory up to use the microservice database
            this.DbFactory = GetConnectionFactory(settings, this.dbName);
            using (var db = this.DbFactory.Open())
            {
                db.CreateTableIfNotExists<User>();
                db.CreateTableIfNotExists<UserGroup>();
                db.CreateTableIfNotExists<UserInUserGroup>();
                db.CreateTableIfNotExists<UserLoginLog>();
                db.CreateTableIfNotExists<Token>();
                db.CreateTableIfNotExists<TrustedAuth>();
                db.CreateTableIfNotExists<Settings>();

                User.CreateAdmin(
                    db,
                    new V2.DTO.User()
                    {
                        email = "info@monosoft.dk",
                        metadata = null,
                        mobile = "none",
                        userId = Guid.NewGuid(),
                        username = "root",
                    });
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServer2016Dialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }
    }
}