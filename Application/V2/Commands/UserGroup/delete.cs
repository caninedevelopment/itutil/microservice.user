﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class delete : DeleteCommand<DTO.UserGroupId>
    {
        public delete() : base("Delete a usergroup")
        {
            this.claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UserGroupId input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var dbusrgrp = db.Select<UserGroup>(p => p.Id == input.usergroupId).FirstOrDefault();

                if (dbusrgrp != null)
                {
                    var users = db.Select<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId).Select(p => p.FK_User);
                    db.Delete<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId);
                    db.Delete<UserGroup>(p => p.Id == input.usergroupId);

                    var affectedusers = new InvalidateUserData()
                    {
                        validUntil = DateTime.Now,
                        userIds = users.ToArray(),
                    };

                    if (affectedusers != null)
                    {
                        EventClient.Instance.RaiseEvent(
                            ITUtil.User.Service.GlobalValues.RouteTokenInvalidateUser,
                            new ITUtil.Common.RabbitMQ.Message.ReturnMessage(affectedusers));
                    }

                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistException("Usergroup does not exist", input.usergroupId.ToString());
                }
            }
        }
    }
}