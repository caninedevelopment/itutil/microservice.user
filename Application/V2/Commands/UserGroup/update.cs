﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ITUtil.Common.RabbitMQ.Message;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class update : UpdateCommand<DTO.UserGroup>
    {
        public update() : base("Updates an usergroup.")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.UserGroup input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var dbusrgrp = db.Select<UserGroup>(p => p.Id == input.usergroupId).FirstOrDefault();

                if (dbusrgrp == null)
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistException("The usergroup does not exist", input.usergroupId.ToString());
                } else
                {
                    dbusrgrp.Name = input.name;
                    dbusrgrp.Claims = input.claims;
                    db.Save<UserGroup>(dbusrgrp);

                    var affectedusers = new InvalidateUserData()
                    {
                        validUntil = DateTime.Now,
                        userIds = db.Select<UserInUserGroup>(p => p.FK_UserGroup == dbusrgrp.Id).Select(p => p.FK_User).ToArray(),
                    };

                    EventClient.Instance.RaiseEvent(ITUtil.User.Service.GlobalValues.RouteTokenInvalidateUser, new ReturnMessage(affectedusers));
                }
            }
        }
    }
}