﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class removeUserFromUsergroup : DeleteCommand<DTO.ChangeUserGroup>
    {
        public removeUserFromUsergroup() : base("Removes an user from an usergroup")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.ChangeUserGroup input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var dbusrgrp = db.Select<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId && p.FK_User == input.userId).FirstOrDefault();

                if (dbusrgrp != null)
                {
                    var affectedusers = new InvalidateUserData()
                    {
                        validUntil = DateTime.Now,
                        userIds = db.Select<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId).Select(p => p.FK_User).ToArray(),
                    };

                    db.Delete<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId && p.FK_User == input.userId);
                    EventClient.Instance.RaiseEvent(
                        ITUtil.User.Service.GlobalValues.RouteTokenInvalidateUser,
                        new ITUtil.Common.RabbitMQ.Message.ReturnMessage(affectedusers));
                } else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistException("User is not in the group", input.userId.ToString());
                }
            }
        }
    }
}