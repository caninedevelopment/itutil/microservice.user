﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class getall : GetCommand<object, DTO.UserGroups>
    {
        public getall() : base("get all usergroups")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override DTO.UserGroups Execute(object input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var dbusrgrps = db.Select<Database.UserGroup>().ToList();

                var usergroups = dbusrgrps.Select(x =>
                     new V2.DTO.ReadUserGroup()
                     {
                         usergroupId = x.Id,
                         name = x.Name,
                         claims = x.Claims,
                         userIds = db.Select<UserInUserGroup>(p => p.FK_UserGroup == x.Id).Select(y => y.FK_User).ToList(),
                 }).ToList();

                var res = new V2.DTO.UserGroups();
                res.usergroups = usergroups;
                return res;
            }
        }
    }
}
