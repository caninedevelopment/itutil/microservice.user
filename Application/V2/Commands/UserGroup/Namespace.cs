﻿using ITUtil.Common.Base;
using ITUtil.Common.Command;
using ITUtil.Common.RabbitMQ.DTO;
using ITUtil.User.Service;
using System.Collections.Generic;

namespace Application.User.V2.Commands.UserGroup
{
    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.Service.User.isAdmin" };

        public Namespace() : base("usergroup", new ProgramVersion("2.0.0.0"))
        {
            this.commands.AddRange(new List<ICommandBase>() {
                new addUserToUsergroup(),
                new create(),
                new delete(),
                new getall(),
                new removeUserFromUsergroup(),
                new update()
            });

            this.events.Add(new ITUtil.Common.Command.EventDefinition<InvalidateUserData>(GlobalValues.RouteTokenInvalidateUser, string.Empty));
        }
    }
}
