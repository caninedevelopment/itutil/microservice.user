﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class addUserToUsergroup : InsertCommand<DTO.ChangeUserGroup>
    {
        public addUserToUsergroup() : base("Add an user to an usergroup")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.ChangeUserGroup input, object args, string initiatingIp)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var dbusrgrp = db.Select<UserInUserGroup>(p => p.FK_UserGroup == input.usergroupId && p.FK_User == input.userId).FirstOrDefault();
                if (dbusrgrp == null)
                {
                    db.Insert<UserInUserGroup>(new UserInUserGroup() { FK_User = input.userId, FK_UserGroup = input.usergroupId });

                    var affectedusers = new InvalidateUserData()
                    {
                        validUntil = DateTime.Now,
                        userIds = new Guid[] { input.userId },
                    };

                    EventClient.Instance.RaiseEvent(
                        ITUtil.User.Service.GlobalValues.RouteTokenInvalidateUser,
                        new ITUtil.Common.RabbitMQ.Message.ReturnMessage(affectedusers));
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("User is already in the group", input.userId.ToString());
                }
            }
        }
    }
}