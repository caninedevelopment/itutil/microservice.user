﻿namespace Application.User.V2.Commands.UserGroup
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class create : InsertCommand<DTO.UserGroup>
    {
        public create() : base("Creates a usergroup")
        {
            this.claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UserGroup input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                if (db.Select<UserGroup>(p=>p.Id == input.usergroupId).Any())
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("The usergroup already exists", input.usergroupId.ToString());
                }

                var dbusrgrp = new UserGroup()
                {
                    Id = input.usergroupId,
                    Name = input.name,
                    Claims = input.claims
                };
                db.Insert<UserGroup>(dbusrgrp);
            }
        }
    }
}