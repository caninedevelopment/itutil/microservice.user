﻿namespace Application.User.V2.Commands.Token
{
    using System;
    using System.Linq;
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ITUtil.Common.RabbitMQ.Message;
    using ITUtil.User.Service;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Command for refresh.
    /// </summary>
    public class refresh : GetCommand<DTO.Token, TokenInfo>
    {
        public refresh()
            : base("Create a refresh token, based on an old token, as long as that token is still valid.", OperationType.Insert) { }

        public override TokenInfo Execute(DTO.Token input, object args, string initiatingIP)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {

                var oldtoken = db.Select<Token>(p => p.Id == input.tokenId && p.ValidUntil > DateTime.Now).FirstOrDefault();

                if (oldtoken == null)
                {
                    throw new ElementDoesNotExistException("Unknow token, can not be refreshed", input.tokenId.ToString());
                }
                else
                {
                    oldtoken.ValidUntil = DateTime.Now.AddHours(6);
                    EventClient.Instance.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, new ReturnMessage(Token.ConvertToTokenData(oldtoken)));
                    db.Update<Token>(oldtoken);
                    return Token.ConvertToTokenData(oldtoken);
                }
            }
        }
    }
}