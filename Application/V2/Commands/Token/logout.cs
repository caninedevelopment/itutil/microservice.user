﻿namespace Application.User.V2.Commands.Token
{
    using System;
    using System.Linq;
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.Event;
    using ITUtil.Common.RabbitMQ.Message;
    using ITUtil.User.Service;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Command for logout.
    /// </summary>
    public class logout : UpdateCommand<DTO.Token>
    {
        public logout()
            : base("Logout the user/token, invalidating the token so it can not be used anymore.") { }

        /// <inheritdoc/>
        public override void Execute(DTO.Token input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var oldtoken = db.Select<Token>(p => p.Id == input.tokenId).FirstOrDefault();
                if (oldtoken != null)
                {
                    oldtoken.ValidUntil = DateTime.Now;
                    db.Update<Token>(oldtoken);

                    EventClient.Instance.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, new ReturnMessage(Token.ConvertToTokenData(oldtoken)));
                }
                else
                {
                    throw new ElementDoesNotExistException("Unknown token", input.tokenId.ToString());
                }
            }
        }
    }
}