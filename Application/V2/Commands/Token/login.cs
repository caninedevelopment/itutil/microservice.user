﻿namespace Application.User.V2.Commands.Token
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Event;
    using ITUtil.Common.RabbitMQ.Message;
    using ITUtil.User.Service;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Command for login.
    /// </summary>
    public class login : GetCommand<DTO.Login, TokenInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="login"/> class.
        /// </summary>
        public login()
            : base("Login the user and returns a token if username and password are correct.", OperationType.Insert) { }

        /// <inheritdoc/>
        public override TokenInfo Execute(DTO.Login input, object args, string initiatingIP)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var usr = db.Select<User>(p => p.Name.Trim().ToLower() == input.username.Trim().ToLower() /*&& p.MD5Password == pwdhash*/).FirstOrDefault();
                if (usr == null)
                {
                    usr = db.Select<User>(p => p.Name.Trim().ToLower() == input.username.Trim().ToLower()).FirstOrDefault();
                    if (usr != null)
                    { // Log the attempt
                        db.Insert<UserLoginLog>(new UserLoginLog()
                        {
                            EventDate = DateTime.Now,
                            Ip = initiatingIP,
                            Success = false,
                            User = usr,
                        });
                    }

                    throw new ITUtil.Common.Base.ElementDoesNotExistException("Invalid credentials", input.username);
                }
                else
                {
                    string pwdhash = HashHelper.CalculateHash(input.password, usr.Id.ToString());
                    if (usr.PWDHash != pwdhash)
                    {
                        db.Insert<UserLoginLog>(new UserLoginLog()
                        {
                            EventDate = DateTime.Now,
                            Ip = initiatingIP,
                            Success = false,
                            User = usr,
                        });

                        throw new ITUtil.Common.Base.ElementDoesNotExistException("Invalid credentials", input.username);

                    }
                    else
                    {

                        db.Insert<UserLoginLog>(new UserLoginLog()
                        {
                            EventDate = DateTime.Now,
                            Ip = initiatingIP,
                            Success = true,
                            User = usr,
                        });

                        var groups = db.Select<UserInUserGroup>(p => p.FK_User == usr.Id).Select(p => p.FK_UserGroup);
                        var orgClaims = db.Select<UserGroup>(p => groups.Contains(p.Id) && p.Claims != null).SelectMany(p => p.Claims);

                        var newtoken = new Token()
                        {
                            FK_User = usr.Id,
                            Id = Guid.NewGuid(),
                            ValidUntil = DateTime.Now.AddHours(6),
                            Claims = new DTO.OrganisationClaims()
                            {
                                orgClaims = orgClaims.Select(p => new TokenInfo.Claim()
                                {
                                    key = p.key,
                                    value = p.value
                                }).ToList(),
                                userClaims = usr.UserClaims,
                            },
                        };
                        db.Insert<Token>(newtoken);

                        foreach (var oldtoken in db.Select<Token>(p =>
                                p.FK_User == usr.Id &&
                                p.ValidUntil >= DateTime.Now).ToList())
                        {
                            oldtoken.ValidUntil = DateTime.Now.AddMinutes(1);
                            EventClient.Instance.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, new ReturnMessage(Token.ConvertToTokenData(oldtoken)));
                        }

                        return Token.ConvertToTokenData(newtoken);
                    }
                }
            }
        }
    }
}
