﻿namespace Application.User.V2.Commands.Token
{
    using System.Collections.Generic;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.User.Service;

    /// <summary>
    /// Namespace for token commands.
    /// </summary>
    public class Namespace : BaseNamespace
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Namespace"/> class.
        /// </summary>
        public Namespace()
            : base("token", new ProgramVersion("2.0.0.0"))
        {
            this.commands.AddRange(new List<ICommandBase>() {
                new login(),
                new logout(),
                new refresh(),
                new verify(),
            });

            this.events.Add(new ITUtil.Common.Command.EventDefinition<TokenInfo>(GlobalValues.RouteTokenInvalidateToken, string.Empty));
        }
    }
}
