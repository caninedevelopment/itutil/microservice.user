﻿namespace Application.User.V2.Commands.Token
{
    using System;
    using System.Linq;
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;

    public class verify : GetCommand<DTO.Token, TokenInfo>
    {
        public verify()
            : base("Verifies a token and return the token details", OperationType.Insert) { }

        public override TokenInfo Execute(DTO.Token input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                Token t = db.Select<Token>(p => p.Id == input.tokenId && p.ValidUntil >= DateTime.Now).FirstOrDefault();

                if (t != null)
                {
                    return Token.ConvertToTokenData(t);
                } else
                {
                    throw new ElementDoesNotExistException("Unknown token", input.tokenId.ToString());
                }
            }
        }
    }
}