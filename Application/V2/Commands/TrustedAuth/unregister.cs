﻿namespace Application.User.V2.Commands.TrustedAuth
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;

    public class unregister : InsertCommand<DTO.UnRegisterTrustedAuth>
    {
        public unregister()
            : base("Unregister a trusted it-util auth server")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.UnRegisterTrustedAuth input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                V2.Database.TrustedAuth.unregister(db, input);
            }
        }
    }
}
