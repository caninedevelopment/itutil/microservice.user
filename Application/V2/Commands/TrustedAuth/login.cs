﻿namespace Application.User.V2.Commands.TrustedAuth
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using Application.User.V2.Database;
    using Application.User.V2.DTO;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using Newtonsoft.Json;
    using ServiceStack.OrmLite;

    public class login : GetCommand<DTO.ExternalLogin, TokenInfo>
    {
        public login()
            : base("Login an external user and returns a token if username and password are correct. Only whitelisted claims will be returned.", OperationType.Insert) { }
        public override TokenInfo Execute(DTO.ExternalLogin input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var trustedEntity = db.Select<V2.Database.TrustedAuth>(p => p.ServerUrl.ToLower() == input.externalUrl.ToLower()).FirstOrDefault();

                if (trustedEntity != null)
                {
                    var json = JsonConvert.SerializeObject(new DTO.Login() { password = input.password, username = input.userName });
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var url = trustedEntity.ServerUrl + "/api/token/v1/login";//version nummer?
                    using var client = new HttpClient();
                    var response = client.PostAsync(url, data).Result;
                    string result = response.Content.ReadAsStringAsync().Result;

                    GatewayResponse remoteresult = JsonConvert.DeserializeObject<GatewayResponse>(result);

                    if (remoteresult.success == false)
                    {
                        throw new Exception(remoteresult.message[0].text);
                    }
                    else
                    {
                        TokenInfo remoteTokenInfo = JsonConvert.DeserializeObject<TokenInfo>(remoteresult.data);//return message wrapper?
                        var whiteliste = trustedEntity.Trust.Select(x => x.key);
                        var acceptedClaims = remoteTokenInfo.claims.Where(p => whiteliste.Contains(p.key) || p.scope == Claim.DataContextEnum.userClaims).ToList();
                        var newtoken = new V2.Database.Token()
                        {
                            FK_User = remoteTokenInfo.userId,
                            Id = remoteTokenInfo.tokenId,
                            ValidUntil = remoteTokenInfo.validUntil,
                            Claims = new OrganisationClaims() 
                            { 
                                orgClaims = acceptedClaims.Where(p=>p.scope== Claim.DataContextEnum.organisationClaims).ToList(),
                                userClaims= acceptedClaims.Where(p => p.scope == Claim.DataContextEnum.userClaims).ToList(),
                            }
                        };
                        db.Insert<V2.Database.Token>(newtoken);

                        var claims = new List<TokenInfo.Claim>();
                        claims.AddRange(newtoken.Claims.orgClaims);
                        claims.AddRange(newtoken.Claims.userClaims);

                        return new TokenInfo() 
                        { 
                            tokenId = newtoken.Id, 
                            userId = newtoken.FK_User, 
                            validUntil = newtoken.ValidUntil, 
                            claims = claims,
                        };
                    }
                } 
                else
                {
                    throw new ElementDoesNotExistException("Server is not trusted", input.externalUrl);
                }
            }
        }
    }
}