﻿namespace Application.User.V2.Commands.TrustedAuth
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;

    public class register : InsertCommand<DTO.RegisterTrustedAuth>
    {
        public register() : base("Register a trusted it-util auth server")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.RegisterTrustedAuth input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                V2.Database.TrustedAuth.register(db, input);
            }
        }
    }
}