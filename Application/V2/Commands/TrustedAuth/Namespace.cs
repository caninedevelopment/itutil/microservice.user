﻿namespace Application.User.V2.Commands.TrustedAuth
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.Service.TrustedAuth.isAdmin" };

        /// <summary>
        /// Initializes a new instance of the <see cref="Namespace"/> class.
        /// </summary>
        public Namespace()
            : base("TrustedAuth", new ProgramVersion("2.0.0.0"))
        {
            this.commands.AddRange( new List<ICommandBase>() {
                    new login(),
                    new register(),
                    new verify(),
            });
        }
    }
}
