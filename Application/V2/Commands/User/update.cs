﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class update : UpdateCommand<DTO.UpdateUser>
    {
        public update() : base("updates an existing user.")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.UpdateUser input, object args, string initiatingIP)
        {
            TokenInfo credentials = args as TokenInfo;
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                if (string.IsNullOrEmpty(input.username))
                {
                    throw new Exception("User username can not be null or empty");
                }

                if (db.Select<User>(p => p.Id != input.userId && p.Name == input.username).Any())
                {
                    throw new ElementAlreadyExistsException("Username is already taken", input.username);
                }

                var dbuser = db.Select<User>(p => p.Id == input.userId).FirstOrDefault();
                if (dbuser != null)
                {
                    dbuser.Email = input.email;
                    dbuser.Mobile = input.mobile;
                    dbuser.Name = input.username;
                    dbuser.UserClaims = input.claims.userClaims;
                    dbuser.Metadata = input.metadata;
                    db.Save(dbuser);
                }
            }
        }
    }
}