﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Collections.Generic;
    using System.Linq;

    public class anonymCreate : InsertCommand<DTO.CreateUser>
    {
        public anonymCreate() : base("create a new external user: note that username must be unique.") { }

        public override void Execute(DTO.CreateUser input, object args, string initiatingIp)
        {
            TokenInfo credentials = args as TokenInfo;
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                // Ensure that email+mobile is valid or NULL
                if (User.ValidEmail(input.email) == false)
                {
                    input.email = null;
                }

                if (User.ValidPhone(input.mobile) == false)
                {
                    input.mobile = null;
                }
                var settings = db.Select<Settings>().FirstOrDefault();
                if (settings == null || string.IsNullOrEmpty(settings.anonymUserGroup))
                {
                    throw new ITUtil.Common.Base.LocalizedException("Please save settings before using anonym create");
                }

                var dbuser = db.Select<User>(p => p.Name.Trim().ToLower() == input.username.Trim().ToLower()).FirstOrDefault();
                if (dbuser == null)
                {
                    dbuser = new User()
                    {
                        Id = input.userId,
                        Name = input.username.Trim().ToLower(),
                        Email = input.email,
                        Mobile = input.mobile,
                        Metadata = new List<DTO.MetaData>(),
                        PWDHash = HashHelper.CalculateHash(input.password, input.userId.ToString())
                    };
                    db.Insert<User>(dbuser);

                    var userInGrp = new UserInUserGroup();
                    userInGrp.FK_User = input.userId;
                    userInGrp.FK_UserGroup = new System.Guid(settings.anonymUserGroup);
                    db.Insert<UserInUserGroup>(userInGrp);
                }
                else
                {
                    throw new ElementAlreadyExistsException("Username already taken", input.username);
                }
            }
        }
    }
}
