﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class resetpassword : GetCommand<DTO.ResetPassword, DTO.ResetPasswordResponse>
    {
        public resetpassword() : base("Get a reset token for forgotten password")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override DTO.ResetPasswordResponse Execute(DTO.ResetPassword input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var user = db.Select<User>(p => p.Email.Trim().ToLower() == input.Email.Trim().ToLower()).FirstOrDefault();
                if (user != null)
                {
                    user.ResetToken = Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                    db.Update<User>(user);

                    return new V2.DTO.ResetPasswordResponse() { ResetToken = user.ResetToken };
                }
                else
                {
                    throw new ElementDoesNotExistException("Unknown email", input.Email);
                }
            }
        }
    }
}