﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using Application.User.V2.DTO;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class getMyData : GetCommand<object, DTO.User>
    {
        public getMyData() : base("Returns data for the current user (based on token).") { }

        public override DTO.User Execute(object input, object args, string initiatingIp)
        {
            var credentials = args as TokenInfo;
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var user = db.Select<Database.User>(p => p.Id == credentials.userId).FirstOrDefault();
                if (user != null)
                {
                    var usrGrps = db.Select<UserInUserGroup>(ug => ug.FK_User == credentials.userId).Select(p => p.FK_UserGroup);
                    var qry = db.Select<Database.UserGroup>(p => usrGrps.Contains(p.Id) && p.Claims != null).Select(p => p.Claims);

                    var res = new V2.DTO.User()
                    {
                        userId = user.Id,
                        username = user.Name,
                        email = user.Email,
                        mobile = user.Mobile,
                        metadata = user.Metadata,
                        claims = new V2.DTO.OrganisationClaims()
                        {
                            orgClaims = qry.SelectMany(v => v.Select(x => new TokenInfo.Claim() { key = x.key, value = x.value})).ToList(),
                            userClaims = user.UserClaims,
                        },
                    };
                    return res;
                }
                else
                {
                    throw new ElementDoesNotExistException("Unknown user", credentials.userId.ToString());
                }
            }
        }
    }
}
