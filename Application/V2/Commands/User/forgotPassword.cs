﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class forgotPassword : UpdateCommand<DTO.ResetPassword>
    {
        public forgotPassword() : base("send a reset token to the user")
        {
        }

        public override void Execute(DTO.ResetPassword input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var settings = db.Select<Settings>().FirstOrDefault();
                if (settings != null)
                {
                    var user = db.Select<User>(p => p.Email.Trim().ToLower() == input.Email.Trim().ToLower()).FirstOrDefault();
                    if (user != null)
                    {
                        user.ResetToken = Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                        db.Update<User>(user);

                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(settings.smtpServerName, settings.smtpPort);
                        if (string.IsNullOrEmpty(settings.smtpUserName) == false)
                        {
                            smtp.Credentials = new System.Net.NetworkCredential(settings.smtpUserName, settings.smtpPassword);
                        }
                        smtp.EnableSsl = settings.smtpPort != 80;

                        var message = new System.Net.Mail.MailMessage();
                        message.From = new System.Net.Mail.MailAddress(settings.ForgotPasswordSenderEmail, settings.ForgotPasswordNameOfSender);
                        message.Body = settings.ForgotPasswordEmailBody.Replace("{0}", user.ResetToken);
                        message.IsBodyHtml = true;
                        message.Subject = settings.ForgotPasswordEmailSubject;
                        message.To.Add(input.Email);
                        smtp.Send(message);
                    }
                    else
                    {
                        throw new ElementDoesNotExistException("Unknown email", input.Email);
                    }
                }
                else
                {
                    throw new ElementDoesNotExistException("Missing smtp setup", "");
                }
            }
        }
    }
}