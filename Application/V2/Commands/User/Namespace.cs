﻿namespace Application.User.V2.Commands.User
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.Service.User.isAdmin" };
        internal static Claim AllowEdit = new Claim() { dataContext = Claim.DataContextEnum.userClaims, description = new LocalizedString[] { new LocalizedString("en", "Organisation can edit userdata") }, key = "Monosoft.Service.User.allowEdit" };

        public Namespace() : base("user", new ProgramVersion("2.0.0.0"))
        {
            this.commands.AddRange(new List<ICommandBase>() {
                new create(),
                new anonymCreate(),
                new forgotPassword(),
                new Setup(),
                new delete(),
                new get(),
                new getall(),
                new getMyData(),
                new resetpassword(),
                new resetpasswordByToken(),
                new update(),
                new updateMyData(),
                new changePassword(),
            });;
        }
    }
}