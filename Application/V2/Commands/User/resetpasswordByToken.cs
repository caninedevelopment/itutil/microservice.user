﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class resetpasswordByToken : UpdateCommand<DTO.ResetpasswordByToken>
    { 
        public resetpasswordByToken() : base("Reset the password by pasing a reset password token") { }

        public override void Execute(DTO.ResetpasswordByToken input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var user = db.Select<User>(p => p.ResetToken != "" && p.ResetToken != null && p.ResetToken == input.Token).FirstOrDefault();
                if (user != null)
                {
                    user.PWDHash = HashHelper.CalculateHash(input.NewPassword, user.Id.ToString());
                    db.Update<User>(user);
                }
                else
                {
                    throw new ElementDoesNotExistException("Unknown token", input.Token);
                }
            }
        }
    }
}