﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class Setup : UpdateCommand<DTO.Settings>
    {
        public Setup() : base("Update the settings for smtp used by forgotpassword and anonymUserGroup. Please note that in the body {0} will be replaced by the actual token")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.Settings input)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var settings = db.Select<Settings>().FirstOrDefault();
                if (settings != null)
                {
                    settings.ForgotPasswordEmailBody = input.Template.Body;
                    settings.ForgotPasswordEmailSubject = input.Template.Subject;
                    settings.ForgotPasswordNameOfSender = input.Template.NameOfSender;
                    settings.ForgotPasswordSenderEmail = input.Template.SenderEmail;
                    settings.smtpPassword = input.smtpPassword;
                    settings.smtpPort = input.smtpPort;
                    settings.smtpServerName = input.smtpServerName;
                    settings.smtpUserName = input.smtpUserName;
                    settings.anonymUserGroup = input.anonymUserGroup;
                    db.Update<Settings>(settings);
                }
                else
                {
                    settings = new Settings();
                    settings.ForgotPasswordEmailBody = input.Template.Body;
                    settings.ForgotPasswordEmailSubject = input.Template.Subject;
                    settings.ForgotPasswordNameOfSender = input.Template.NameOfSender;
                    settings.ForgotPasswordSenderEmail = input.Template.SenderEmail;
                    settings.smtpPassword = input.smtpPassword;
                    settings.smtpPort = input.smtpPort;
                    settings.smtpServerName = input.smtpServerName;
                    settings.smtpUserName = input.smtpUserName;
                    settings.anonymUserGroup = input.anonymUserGroup;
                    db.Insert<Settings>(settings);
                }
            }
        }
    }
}
