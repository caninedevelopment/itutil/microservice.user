﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class delete : DeleteCommand<DTO.DeleteUser>
    {
        public delete() : base("deletes an existing user.")
        {
            this.claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.DeleteUser input, object args, string initiatingIp)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var user = db.Select<User>(p => p.Id == input.userId).FirstOrDefault();

                if (user == null)
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistException("The user does not exists", input.userId.ToString());
                }

                db.Delete<User>(p => p.Id == user.Id);
            }
        }
    }
}
