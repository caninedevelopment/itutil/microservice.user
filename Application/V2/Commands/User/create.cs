﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Collections.Generic;
    using System.Linq;

    public class create : InsertCommand<DTO.CreateUser>
    {
        public create() : base("create a new user: note that username must be unique.")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.CreateUser input, object args, string initiatingIp)
        {
            TokenInfo credentials = args as TokenInfo;
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                // Ensure that email+mobile is valid or NULL
                if (User.ValidEmail(input.email) == false)
                {
                    input.email = null;
                }

                if (User.ValidPhone(input.mobile) == false)
                {
                    input.mobile = null;
                }

                var dbuser = db.Select<User>(p => p.Name.Trim().ToLower() == input.username.Trim().ToLower()).FirstOrDefault();
                if (dbuser == null)
                {
                    dbuser = new User()
                    {
                        Id = input.userId,
                        Name = input.username.Trim().ToLower(),
                        Email = input.email.Trim().ToLower(),
                        Mobile = input.mobile,
                        Metadata = input.metadata,
                        PWDHash = HashHelper.CalculateHash(input.password, input.userId.ToString())
                    };
                    db.Insert<User>(dbuser);
                }
                else
                {
                    throw new ElementAlreadyExistsException("Username already taken", input.username);
                }
            }
        }
    }
}
