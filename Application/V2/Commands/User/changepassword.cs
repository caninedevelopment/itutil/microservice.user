﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Linq;

    public class changePassword : UpdateCommand<DTO.changePassword>
    {
        public changePassword() : base("change the password of the current user by token") { }

        public override void Execute(DTO.changePassword input, object args, string initiatingIp)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var userId = (args as TokenInfo).userId;
                var pwd = HashHelper.CalculateHash(input.oldPassword, userId.ToString());
                var user = db.Select<User>(p => p.Id == userId && p.PWDHash == pwd).FirstOrDefault();
                if (user != null)
                {
                    user.PWDHash = HashHelper.CalculateHash(input.newPassword, userId.ToString());
                    db.Update<User>(user);
                }
                else
                {
                    throw new ElementDoesNotExistException("Username or password is invalid", userId.ToString());
                }
            }
        }
    }
}
