﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System.Collections.Generic;
    using System.Linq;

    public class getall : GetCommand<object, DTO.Users>
    {
        public getall() : base("Get all users")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        public override DTO.Users Execute(object input, object args, string initiatingIP)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                List<V2.DTO.User> res = new List<V2.DTO.User>();
                foreach (var usr in db.Select<User>().ToList())
                {
                    var usrGrps = db.Select<UserInUserGroup>(ug => ug.FK_User == usr.Id ).Select(p => p.FK_UserGroup);
                    var qry = db.Select<UserGroup>(p => usrGrps.Contains(p.Id)).Select(p => p.Claims);

                    res.Add(new V2.DTO.User()
                    {
                        email = usr.Email,
                        mobile = usr.Mobile,
                        userId = usr.Id,
                        username = usr.Name,
                        metadata = usr.Metadata,
                        claims = new V2.DTO.OrganisationClaims
                        {
                            orgClaims = qry == null ? new List<TokenInfo.Claim>() : qry.SelectMany(p => p.Select(x => new TokenInfo.Claim() { key = x.key, value = x.value})).ToList(),
                            userClaims = usr.UserClaims
                        },
                    });
                }

                return new DTO.Users() { users = res };
            }
        }
    }
}
