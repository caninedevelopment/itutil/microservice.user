﻿namespace Application.User.V2.Commands.User
{
    using Application.User.V2.Database;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System;
    using System.Linq;

    public class updateMyData : UpdateCommand<DTO.UpdateUser>
    {
        public updateMyData() : base("updates the current user (based on token).") { }

        public override void Execute(DTO.UpdateUser input, object args, string initiatingIP)
        {
            using (var db = DataContext.Instance.DbFactory.Open())
            {
                var credentials = args as TokenInfo;

                if ((args as TokenInfo).userId == input.userId)
                {
                    if (string.IsNullOrEmpty(input.username))
                    {
                        throw new Exception("User username can not be null or empty");
                    }

                    if (db.Select<User>(p=> p.Name == input.username && p.Id != input.userId).Any())
                    {
                        throw new ElementAlreadyExistsException("The username is already in use", input.username);
                    }

                    var dbuser = db.Select<User>(p => p.Id == input.userId).FirstOrDefault();
                    if (dbuser != null)
                    {
                        dbuser.Email = input.email;
                        dbuser.Mobile = input.mobile;
                        dbuser.Name = input.username;
                        dbuser.UserClaims = input.claims.userClaims;
                        dbuser.Metadata = input.metadata;
                        db.Save(dbuser);
                    }
                }
                else
                {
                    throw new ElementDoesNotExistException("Usertoken and user is a mismatch, please use the update command instead", input.userId.ToString());
                }
            }
        }
    }
}