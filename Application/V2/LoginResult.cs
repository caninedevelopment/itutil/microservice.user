﻿using ITUtil.Common.RabbitMQ.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.V2
{
    public class LoginResult
    {
        public List<TokenInfo> OldToken { get; set; }
        public TokenInfo NewToken { get; set; }
    }
}
